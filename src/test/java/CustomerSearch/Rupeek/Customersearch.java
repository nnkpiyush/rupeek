package CustomerSearch.Rupeek;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Customersearch
{
	static String tok="";
	@Test(priority=1)
	public static void gettoken()
	{
		
	RestAssured.baseURI="http://13.126.80.194:8080";
	RequestSpecification req=RestAssured.given();
	JSONObject reqparam=new JSONObject();
	reqparam.put("username", "rupeek");
	reqparam.put("password","password");
	req.header("content-type","application/json");
	req.body(reqparam.toJSONString());
	Response res=req.post("/authenticate");
	int statuscode=res.getStatusCode();
	Assert.assertEquals(statuscode, 200);
	System.out.println("API check 1");
	String token=res.asString();
	JsonPath j=new JsonPath(token);
	tok=j.getString("token");
	System.out.println("result "+tok);
	System.out.println(token);
	}
	
	@Test(priority=2)
	public void customerrecord()
	{
		RestAssured.baseURI="http://13.126.80.194:8080";
		RequestSpecification req=RestAssured.given();
		req.header("Authorization", "Bearer "+tok);
		Response res=req.get("/api/v1/users");
		int statuscode=res.getStatusCode();
		Assert.assertEquals(statuscode, 200);
		System.out.println("API check 2");
		String respo=res.asString();
		System.out.println(respo);
		
	}
	@Test(priority=3)
	public void customerrecordbymobile()
	{
		String mobile="8037602400";
		RestAssured.baseURI="http://13.126.80.194:8080";
		RequestSpecification req=RestAssured.given();
		req.header("Authorization", "Bearer "+tok);
		Response res=req.get("/api/v1/users/"+mobile);
		int statuscode=res.getStatusCode();
		Assert.assertEquals(statuscode, 200);
		System.out.println("API check 3");
		String respo=res.asString();
		System.out.println(respo);
		JsonPath j=new JsonPath(respo);
		Assert.assertEquals(j.getString("first_name"), "Aliko");
		Assert.assertEquals(j.getString("last_name"), "Dangote");
		
	
	}
	
	}
	
	
	
	
	
